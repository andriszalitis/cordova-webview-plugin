package webview;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.SupportActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;



public class WebViewActivity extends SupportActivity {

    private WebView mWebView;
    private boolean loaded=false;
    private String urlContext;
    private static final String webViewClosed="webViewClosed";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        int layoutID = getResources().getIdentifier("wview", "layout", getPackageName());
//        getResources().getLayout()

        setContentView(getResources().getIdentifier("wview", "layout", getPackageName()));
        String _url = getIntent().getStringExtra(webview.WebView.DATA_WEBPAGE_URL);
        urlContext = getIntent().getStringExtra(webview.WebView.DATA_CONTEXT_URL);
        String _title = getIntent().getStringExtra(webview.WebView.DATA_TITLE);

        ((TextView) findViewById(getResources().getIdentifier("title_text", "id", getPackageName()))).setText(_title);
        findViewById(getResources().getIdentifier("close_button", "id", getPackageName())).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webview.WebView.sendResult(webViewClosed);
                finish();
            }
        });
        showWebView(_url);
    }

    @Override
    public void onBackPressed() {
        webview.WebView.sendResult(webViewClosed);
        finish();
    }

    private  void showWebView(String _url) {
        if(!loaded){
            mWebView = (WebView) findViewById(getResources().getIdentifier("wview", "id", getPackageName()));
            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String _redirectTo) {
                    String _searchFor = String.format("%s:", urlContext);
                    if(_redirectTo.toLowerCase().contains(_searchFor.toLowerCase())){
                        _redirectTo = _redirectTo.replace(String.format("%s://", urlContext), "").replace(String.format("%s:", urlContext), "");
                        webview.WebView.sendResult(_redirectTo);
                        finish();
                        return true; // Don't allow redirect
                    }
                    return false; // Allow redirect
                }
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    findViewById(getResources().getIdentifier("progress_round", "id", getPackageName())).setVisibility(View.GONE);
                }
            });
            mWebView.setVisibility(View.VISIBLE);
            mWebView.clearCache(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                WebView.setWebContentsDebuggingEnabled(true);
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            mWebView.loadUrl(_url);
        }
    }
}