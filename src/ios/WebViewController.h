#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@class WebViewController;

@protocol WebViewControllerDelegate <NSObject>

- (void)webViewControllerDidFinish:(WebViewController *_Nonnull)webViewController;
- (void)webViewControllerDidCaptureCommand:(WebViewController *_Nonnull)webViewController command:(NSString *_Nonnull)command;
- (void)webViewControllerDidFailToLoadContent:(WebViewController *_Nonnull)webViewController;

@end

@interface WebViewController : UIViewController <WKNavigationDelegate>

@property (weak, nonatomic) _Nullable id<WebViewControllerDelegate> delegate;
@property (copy, nonatomic, nonnull) NSURL *url;
@property (copy, nonatomic, nonnull) NSString *callbackScheme;

@end
